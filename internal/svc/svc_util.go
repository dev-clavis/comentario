package svc

import "github.com/op/go-logging"

// logger represents a package-wide logger instance
var logger = logging.MustGetLogger("svc")
